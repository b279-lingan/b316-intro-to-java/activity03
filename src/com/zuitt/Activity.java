package com.zuitt;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num = 0;

        System.out.println("Enter an integer whose factorial is computed:");

        try {
            num = input.nextInt();

            // Check for zero and negative values
            if (num == 0) {
                System.out.println("The factorial of 0 is: 1");
            } else if (num < 0) {
                System.out.println("Cannot compute factorial for negative numbers.");
            } else {

                // Calculate factorial using a do-while loop
                // int factorial = 1;
                // int i = 1;
                // do {
                // factorial *= i;
                // i++;
                // } while (i <= num);

                // System.out.println("The factorial of " + num + " is: " + factorial);

                int factorial = 1;
                for (int i = 1; i <= num; i++) {
                    factorial *= i;
                }

                System.out.println("The factorial of " + num + " is: " + factorial);
            }
        } catch (Exception e) {
            System.out.println("Invalid input.");
            e.printStackTrace();
        } finally {
            input.close();
        }
    }
}






